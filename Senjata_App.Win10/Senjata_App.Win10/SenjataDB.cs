using MySql.Data.MySqlClient;
using Senjata_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Senjata_App.Win10
{
    class SenjataDB
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public SenjataDB(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Senjata MappingRowToObject(MySqlDataReader dtr)
        {
            int id_senjata = dtr["id_senjata"] is DBNull ? 0 : Convert.ToInt32(dtr["id_senjata"].ToString());
            string jenis = dtr["jenis"] is DBNull ? string.Empty : dtr["jenis"].ToString();
            string merk = dtr["merk"] is DBNull ? string.Empty : dtr["merk"].ToString();
            string amunisi = dtr["amunisi"] is DBNull ? string.Empty : dtr["amunisi"].ToString();
            int jumlah = dtr["jumlah"] is DBNull ? 0 : Convert.ToInt32(dtr["jumlah"].ToString());
            
            return new Senjata()
            {
                id_senjata = id_senjata,
				jenis = jenis,
                merk = merk,
                amunisi = amunisi,
                jumlah = jumlah
            };
        }

        public List<Senjata> GetAll() //read
        {
            List<Senjata> daftarsjt = new List<Senjata>();

            strSql = "SELECT * FROM senjata";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarsjt.Add(MappingRowToObject(dtr));
                    }
                }
            }
            return daftarsjt;
        }

        public int Save(Senjata sjt)
        {
            strSql = "INSERT INTO senjata (id_senjata, jenis, merk, amunisi, jumlah) VALUES(@1, @2, @3, @4, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", sjt.id_senjata);
                cmd.Parameters.AddWithValue("@2", sjt.jenis);
                cmd.Parameters.AddWithValue("@3", sjt.merk);
                cmd.Parameters.AddWithValue("@4", sjt.amunisi);
                cmd.Parameters.AddWithValue("@5", sjt.jumlah);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Senjata sjt)
        {
            strSql = "DELETE FROM senjata WHERE id_senjata = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", sjt.id_senjata);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Senjata sjt)
        {
            strSql = "UPDATE senjata SET id_senjata = @1, jenis = @2, merk = @3, amunisi = @4, jumlah = @5 WHERE id_senjata = @8";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", sjt.id_senjata);
                cmd.Parameters.AddWithValue("@2", sjt.jenis);
                cmd.Parameters.AddWithValue("@3", sjt.merk);
                cmd.Parameters.AddWithValue("@4", sjt.amunisi);
                cmd.Parameters.AddWithValue("@5", sjt.jumlah);
				cmd.Parameters.AddWithValue("@8", sjt.id_senjata);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
