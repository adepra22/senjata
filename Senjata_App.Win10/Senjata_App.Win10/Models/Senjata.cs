namespace Senjata_App.Win10.Models {
    public class Senjata {
        public int id_senjata { get; set; }
        public string jenis { get; set; }
        public string merk { get; set; }
        public string amunisi { get; set; }
        public int jumlah { get; set; }
    }
}
