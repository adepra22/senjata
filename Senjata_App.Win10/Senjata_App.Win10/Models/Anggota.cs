﻿namespace Senjata_App.Win10.Models {
    public class Anggota {
        public int id_anggota { get; set; }
        public string no_ktp { get; set; }
        public string nama { get; set; }
        public string jk { get; set; }
        public string ttl { get; set; }
        public string no_telp { get; set; }
        public string alamat { get; set; }
    }
}
