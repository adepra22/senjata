﻿namespace Senjata_App.Win10.Models {
    public class Pengembalian {
        public int id_pengembalian { get; set; }
        public int id_peminjaman { get; set; }
        public int id_petugas { get; set; }
        public string tanggal { get; set; }
    }
}
