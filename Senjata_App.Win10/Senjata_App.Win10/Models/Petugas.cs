﻿namespace Senjata_App.Win10.Models {
    public class Petugas {
        public int id_petugas { get; set; }
        public string nama { get; set; }
        public string no_telp { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
