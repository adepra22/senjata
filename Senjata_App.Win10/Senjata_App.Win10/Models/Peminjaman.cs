﻿namespace Senjata_App.Win10.Models {
    public class Peminjaman {
        public int id_peminjaman { get; set; }
        public int id_anggota { get; set; }
        public int id_petugas { get; set; }
        public int id_senjata { get; set; }
        public string tanggal { get; set; }
    }
}
