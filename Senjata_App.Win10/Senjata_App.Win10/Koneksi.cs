﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Senjata_App.Win10
{
    class koneksi
    {
        private MySqlConnection konek = null;
        private static koneksi dbkoneksi = null;

        private koneksi()
        {
            if (konek == null)
            {
                string server = "localhost";
                string database = "senjata";
                string user = "root";
                string password = "";
                string strkoneksi = "SERVER=" + server + ";DATABASE=" + database + ";UID=" + user + ";PASSWORD=" + password;
                konek = new MySqlConnection(strkoneksi);
                konek.Open();

            }
        }

        public static koneksi GetInstance()
        {
            if (dbkoneksi == null)
            {
                dbkoneksi = new koneksi();
            }

            return dbkoneksi;
        }

        public MySqlConnection GetConnection()
        {
            return this.konek;
        }
    }
}
