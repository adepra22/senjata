﻿using System.Windows;
using Senjata_App.Win10.ViewModels;
using Senjata_App.Win10.Models;
using Peminjaman_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class PeminjamanForm : Window {
        public PeminjamanForm(PeminjamanViewModel vm) {
            InitializeComponent();
            if (vm.ModelPeminjaman == null) {
                vm.ModelPeminjaman = new Peminjaman();
                BtnSave.Visibility = Visibility.Visible;
                BtnUpdate.Visibility = Visibility.Hidden;
                BtnDelete.Visibility = Visibility.Hidden;
            } else {
                BtnSave.Visibility = Visibility.Hidden;
                BtnUpdate.Visibility = Visibility.Visible;
                BtnDelete.Visibility = Visibility.Visible;
            }
            DataContext = vm;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
