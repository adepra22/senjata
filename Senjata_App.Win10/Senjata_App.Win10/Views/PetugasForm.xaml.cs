﻿using System.Windows;
using Senjata_App.Win10.ViewModels;
using Bci2D_App.Win10.ViewModels;
using Senjata_App.Win10.Models;
using Petugas_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class PetugasForm : Window {
        public PetugasForm(PetugasViewModel vm) {
            InitializeComponent();
            if (vm.ModelPetugas == null) {
                vm.ModelPetugas = new Petugas();
                BtnSave.Visibility = Visibility.Visible;
                BtnUpdate.Visibility = Visibility.Hidden;
                BtnDelete.Visibility = Visibility.Hidden;
            } else {
                BtnSave.Visibility = Visibility.Hidden;
                BtnUpdate.Visibility = Visibility.Visible;
                BtnDelete.Visibility = Visibility.Visible;
            }
            DataContext = vm;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
