﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Peminjaman_App.Win10.ViewModels;
using Senjata_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class PeminjamanView : UserControl {
        public PeminjamanView() {
            InitializeComponent();
            vm = new PeminjamanViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataPeminjaman;
                if (form != null) {
                    form.Close();
                }
                vm.ModelPeminjaman = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private PeminjamanViewModel vm;
        private PeminjamanForm form;

        private async Task InitFormAsync() {
            await Task.Delay(0);
            form = new PeminjamanForm(vm);
            form.ShowDialog();
        }

        private async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            await Task.Delay(0);
            if (vm.ModelPeminjaman != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private async void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelPeminjaman = null;
            await InitFormAsync();
        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e) {
            await InitFormAsync();
        }

        private async void BtnReset_Click(object sender, RoutedEventArgs e) {
            await Task.Delay(0);
            vm.ModelPeminjaman = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
