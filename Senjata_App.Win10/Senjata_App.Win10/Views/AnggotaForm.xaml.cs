﻿using System.Windows;
using Senjata_App.Win10.ViewModels;
using Senjata_App.Win10.Models;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class AnggotaForm : Window {
        public AnggotaForm(AnggotaViewModel vm) {
            InitializeComponent();
            if (vm.ModelAnggota == null) {
                vm.ModelAnggota = new Anggota();
                BtnSave.Visibility = Visibility.Visible;
                BtnUpdate.Visibility = Visibility.Hidden;
                BtnDelete.Visibility = Visibility.Hidden;
            } else {
                BtnSave.Visibility = Visibility.Hidden;
                BtnUpdate.Visibility = Visibility.Visible;
                BtnDelete.Visibility = Visibility.Visible;
            }
            DataContext = vm;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
