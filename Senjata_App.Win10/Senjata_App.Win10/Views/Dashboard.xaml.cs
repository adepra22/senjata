﻿using System.Windows;

namespace Senjata_App.Win10.Views.Home {
    public partial class Dashboard : Window {
        public Dashboard() {
            InitializeComponent();
        }

        private void BtnSenjata_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Jeniss.SenjataView());
        }

        private void BtnAnggota_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Jeniss.AnggotaView());
        }

        private void BtnPetugas_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Jeniss.PetugasView());
        }

        private void BtnPengembalian_Click(object sender, RoutedEventArgs e) {
			App.ViewRouting(false);
            App.ViewRouting(true, new Jeniss.PengembalianView());
        }

        private void BtnPeminjaman_Click(object sender, RoutedEventArgs e) {
			App.ViewRouting(false);
            App.ViewRouting(true, new Jeniss.PeminjamanView());
        }

        private void MnuExit_Click(object sender, RoutedEventArgs e) {
            Application.Current.Shutdown();
        }
    }
}
