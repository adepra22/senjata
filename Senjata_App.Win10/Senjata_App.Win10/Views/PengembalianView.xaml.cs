﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Pengembalian_App.Win10.ViewModels;
using Senjata_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class PengembalianView : UserControl {
        public PengembalianView() {
            InitializeComponent();
            vm = new PengembalianViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataPengembalian;
                if (form != null) {
                    form.Close();
                }
                vm.ModelPengembalian = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private PengembalianViewModel vm;
        private PengembalianForm form;

        private async Task InitFormAsync() {
            await Task.Delay(0);
            form = new PengembalianForm(vm);
            form.ShowDialog();
        }

        private async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            await Task.Delay(0);
            if (vm.ModelPengembalian != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private async void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelPengembalian = null;
            await InitFormAsync();
        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e) {
            await InitFormAsync();
        }

        private async void BtnReset_Click(object sender, RoutedEventArgs e) {
            await Task.Delay(0);
            vm.ModelPengembalian = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
