﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Senjata_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class AnggotaView : UserControl {
        public AnggotaView() {
            InitializeComponent();
            vm = new AnggotaViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataAnggota;
                if (form != null) {
                    form.Close();
                }
                vm.ModelAnggota = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private AnggotaViewModel vm;
        private AnggotaForm form;

        private async Task InitFormAsync() {
            await Task.Delay(0);
            form = new AnggotaForm(vm);
            form.ShowDialog();
        }

        private async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            await Task.Delay(0);
            if (vm.ModelAnggota != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private async void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelAnggota = null;
            await InitFormAsync();
        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e) {
            await InitFormAsync();
        }

        private async void BtnReset_Click(object sender, RoutedEventArgs e) {
            await Task.Delay(0);
            vm.ModelAnggota = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
