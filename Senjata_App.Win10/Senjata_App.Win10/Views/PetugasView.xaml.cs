﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Petugas_App.Win10.ViewModels;
using Senjata_App.Win10.ViewModels;

namespace Senjata_App.Win10.Views.Jeniss {
    public partial class PetugasView : UserControl {
        public PetugasView() {
            InitializeComponent();
            vm = new PetugasViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataPetugas;
                if (form != null) {
                    form.Close();
                }
                vm.ModelPetugas = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private PetugasViewModel vm;
        private PetugasForm form;

        private async Task InitFormAsync() {
            await Task.Delay(0);
            form = new PetugasForm(vm);
            form.ShowDialog();
        }

        private async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            await Task.Delay(0);
            if (vm.ModelPetugas != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private async void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelPetugas = null;
            await InitFormAsync();
        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e) {
            await InitFormAsync();
        }

        private async void BtnReset_Click(object sender, RoutedEventArgs e) {
            await Task.Delay(0);
            vm.ModelPetugas = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
