﻿using MySql.Data.MySqlClient;
using Senjata_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Senjata_App.Win10
{
    class AnggotaDB
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public AnggotaDB(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Anggota MappingRowToObject(MySqlDataReader dtr)
        {
            int id_anggota = dtr["id_anggota"] is DBNull ? 0 : Convert.ToInt32(dtr["id_anggota"].ToString());
            string no_ktp = dtr["no_ktp"] is DBNull ? string.Empty : dtr["no_ktp"].ToString();
            string nama = dtr["nama"] is DBNull ? string.Empty : dtr["nama"].ToString();
            string jk = dtr["jk"] is DBNull ? string.Empty : dtr["jk"].ToString();
            string ttl = dtr["ttl"] is DBNull ? string.Empty : dtr["ttl"].ToString();
            string no_telp = dtr["no_telp"] is DBNull ? string.Empty : dtr["no_telp"].ToString();
            string alamat = dtr["alamat"] is DBNull ? string.Empty : dtr["alamat"].ToString();


            return new Anggota()
            {
                id_anggota = id_anggota,
				no_ktp = no_ktp,
                nama = nama,
                jk = jk,
                ttl = ttl,
                no_telp = no_telp,
                alamat = alamat
            };
        }

        public List<Anggota> GetAll() //read
        {
            List<Anggota> daftaragt = new List<Anggota>();

            strSql = "SELECT * FROM anggota";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftaragt.Add(MappingRowToObject(dtr));
                    }
                }
            }
            return daftaragt;
        }

        public int Save(Anggota agt)
        {
            strSql = "INSERT INTO anggota (id_anggota, no_ktp, nama, jk, ttl, no_telp, alamat) VALUES(@1, @2, @3, @4, @5, @6, @7)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", agt.id_anggota);
                cmd.Parameters.AddWithValue("@2", agt.no_ktp);
                cmd.Parameters.AddWithValue("@3", agt.nama);
                cmd.Parameters.AddWithValue("@4", agt.jk);
                cmd.Parameters.AddWithValue("@5", agt.ttl);
                cmd.Parameters.AddWithValue("@6", agt.no_telp);
                cmd.Parameters.AddWithValue("@7", agt.alamat);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Anggota agt)
        {
            strSql = "DELETE FROM anggota WHERE id_anggota = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", agt.id_anggota);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Anggota agt)
        {
            strSql = "UPDATE anggota SET id_anggota = @1, no_ktp = @2, nama = @3, jk = @4, ttl = @5, no_telp = @6, alamat = @7 WHERE id_anggota = @8";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", agt.id_anggota);
                cmd.Parameters.AddWithValue("@2", agt.no_ktp);
                cmd.Parameters.AddWithValue("@3", agt.nama);
                cmd.Parameters.AddWithValue("@4", agt.jk);
                cmd.Parameters.AddWithValue("@5", agt.ttl);
                cmd.Parameters.AddWithValue("@6", agt.no_telp);
                cmd.Parameters.AddWithValue("@7", agt.alamat);
				cmd.Parameters.AddWithValue("@8", agt.id_anggota);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
