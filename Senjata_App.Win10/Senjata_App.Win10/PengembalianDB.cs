using MySql.Data.MySqlClient;
using Senjata_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Senjata_App.Win10
{
    class PengembalianDB
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public PengembalianDB(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Pengembalian MappingRowToObject(MySqlDataReader dtr)
        {
            int id_pengembalian = dtr["id_pengembalian"] is DBNull ? 0 : Convert.ToInt32(dtr["id_pengembalian"].ToString());
            int id_peminjaman = dtr["id_peminjaman"] is DBNull ? 0 : Convert.ToInt32(dtr["id_peminjaman"].ToString());
            int id_petugas = dtr["id_petugas"] is DBNull ? 0 : Convert.ToInt32(dtr["id_petugas"].ToString());
            string tanggal = dtr["tanggal"] is DBNull ? string.Empty : dtr["tanggal"].ToString();
            
            return new Pengembalian()
            {
                id_pengembalian = id_pengembalian,
				id_peminjaman = id_peminjaman,
                id_petugas = id_petugas,
                tanggal = tanggal
            };
        }

        public List<Pengembalian> GetAll() //read
        {
            List<Pengembalian> daftarkbl = new List<Pengembalian>();

            strSql = "SELECT * FROM pengembalian";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarkbl.Add(MappingRowToObject(dtr));
                    }
                }
            }
            return daftarkbl;
        }

        public int Save(Pengembalian kbl)
        {
            strSql = "INSERT INTO pengembalian (id_pengembalian, id_peminjaman, id_petugas, tanggal) VALUES(@1, @2, @3, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", kbl.id_pengembalian);
                cmd.Parameters.AddWithValue("@2", kbl.id_peminjaman);
                cmd.Parameters.AddWithValue("@3", kbl.id_petugas);
                cmd.Parameters.AddWithValue("@5", kbl.tanggal);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Pengembalian kbl)
        {
            strSql = "DELETE FROM pengembalian WHERE id_pengembalian = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", kbl.id_pengembalian);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Pengembalian kbl)
        {
            strSql = "UPDATE pengembalian SET id_pengembalian = @1, id_peminjaman = @2, id_petugas = @3, tanggal = @5 WHERE id_pengembalian = @8";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", kbl.id_pengembalian);
                cmd.Parameters.AddWithValue("@2", kbl.id_peminjaman);
                cmd.Parameters.AddWithValue("@3", kbl.id_petugas);
                cmd.Parameters.AddWithValue("@5", kbl.tanggal);
				cmd.Parameters.AddWithValue("@8", kbl.id_pengembalian);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
