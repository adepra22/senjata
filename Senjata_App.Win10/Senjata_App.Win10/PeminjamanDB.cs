using MySql.Data.MySqlClient;
using Senjata_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Senjata_App.Win10
{
    class PeminjamanDB
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public PeminjamanDB(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Peminjaman MappingRowToObject(MySqlDataReader dtr)
        {
            int id_peminjaman = dtr["id_peminjaman"] is DBNull ? 0 : Convert.ToInt32(dtr["id_peminjaman"].ToString());
            int id_anggota = dtr["id_anggota"] is DBNull ? 0 : Convert.ToInt32(dtr["id_anggota"].ToString());
            int id_petugas = dtr["id_petugas"] is DBNull ? 0 : Convert.ToInt32(dtr["id_petugas"].ToString());
            int id_senjata = dtr["id_senjata"] is DBNull ? 0 : Convert.ToInt32(dtr["id_senjata"].ToString());
            string tanggal = dtr["tanggal"] is DBNull ? string.Empty : dtr["tanggal"].ToString();
            
            return new Peminjaman()
            {
                id_peminjaman = id_peminjaman,
				id_anggota = id_anggota,
                id_petugas = id_petugas,
                id_senjata = id_senjata,
                tanggal = tanggal
            };
        }

        public List<Peminjaman> GetAll() //read
        {
            List<Peminjaman> daftarpjm = new List<Peminjaman>();

            strSql = "SELECT * FROM peminjaman";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarpjm.Add(MappingRowToObject(dtr));
                    }
                }
            }
            return daftarpjm;
        }

        public int Save(Peminjaman pjm)
        {
            strSql = "INSERT INTO peminjaman (id_peminjaman, id_anggota, id_petugas, id_senjata, tanggal) VALUES(@1, @2, @3, @4, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", pjm.id_peminjaman);
                cmd.Parameters.AddWithValue("@2", pjm.id_anggota);
                cmd.Parameters.AddWithValue("@3", pjm.id_petugas);
                cmd.Parameters.AddWithValue("@4", pjm.id_senjata);
                cmd.Parameters.AddWithValue("@5", pjm.tanggal);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Peminjaman pjm)
        {
            strSql = "DELETE FROM peminjaman WHERE id_peminjaman = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", pjm.id_peminjaman);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Peminjaman pjm)
        {
            strSql = "UPDATE peminjaman SET id_peminjaman = @1, id_anggota = @2, id_petugas = @3, id_senjata = @4, tanggal = @5 WHERE id_peminjaman = @8";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", pjm.id_peminjaman);
                cmd.Parameters.AddWithValue("@2", pjm.id_anggota);
                cmd.Parameters.AddWithValue("@3", pjm.id_petugas);
                cmd.Parameters.AddWithValue("@4", pjm.id_senjata);
                cmd.Parameters.AddWithValue("@5", pjm.tanggal);
				cmd.Parameters.AddWithValue("@8", pjm.id_peminjaman);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
