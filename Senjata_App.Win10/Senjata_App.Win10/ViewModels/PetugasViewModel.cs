﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input;
using Bci2D_App.Win10.ViewModels;
using Senjata_App.Win10.Models;
using Senjata_App.Win10.Views.Jeniss;
using Senjata_App.Win10;

namespace Petugas_App.Win10.ViewModels {
    public class PetugasViewModel : BaseViewModel {
        koneksi koneksi;
        PetugasDB Petugas;

        public PetugasViewModel()
        {
            koneksi = koneksi.GetInstance();
            Petugas = new PetugasDB(koneksi.GetConnection());

            dataPetugas = new ObservableCollection<Petugas>();
            modelPetugas = new Petugas();

            CreateCommand = new Command(async () => await CreatePetugasAsync());
            UpdateCommand = new Command(async () => await UpdatePetugasAsync());
            DeleteCommand = new Command(async () => await DeletePetugasAsync());
            ReadCommand = new Command(async () => await ReadPetugasAsync(true));
            ReadCommand.Execute(null);
        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Petugas> DataPetugas // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return dataPetugas;
            }

            set
            {
                SetProperty(ref dataPetugas, value); // property yang ada di base view models
            }
        }

        public Petugas ModelPetugas
        {
            get
            {
                return modelPetugas;
            }

            set
            {
                SetProperty(ref modelPetugas, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Petugas> dataPetugas;
        private Petugas modelPetugas;


        private async Task InitPetugasAsync()
        {
            var data = Petugas.GetAll();

            await Task.Run(() => {
                DataPetugas = new ObservableCollection<Petugas>(data);
            });
        }

        private async Task ReadPetugasAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitPetugasAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Petugas> ReadPetugasAsync(int id_petugas)
        {
            await Task.Delay(0);
            return DataPetugas.Where(model => model.id_petugas.Equals(id_petugas)).SingleOrDefault();
        }

        private async Task CreatePetugasAsync()
        {
            Petugas.Save(ModelPetugas);
            await ReadPetugasAsync(true);
        }

        private async Task UpdatePetugasAsync()
        {
            var data = ModelPetugas;
            Petugas.Update(data);
            await ReadPetugasAsync(true);
        }

        private async Task DeletePetugasAsync()
        {
            Petugas.Delete(ModelPetugas);
            await ReadPetugasAsync(true);
        }
    }
}
