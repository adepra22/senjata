﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Senjata_App.Win10.Models;
using Bci2D_App.Win10.ViewModels;
using Senjata_App.Win10;

namespace Peminjaman_App.Win10.ViewModels {
    public class PeminjamanViewModel : BaseViewModel {
        koneksi koneksi;
        PeminjamanDB Peminjaman;

        public PeminjamanViewModel()
        {
            koneksi = koneksi.GetInstance();
            Peminjaman = new PeminjamanDB(koneksi.GetConnection());

            dataPeminjaman = new ObservableCollection<Peminjaman>();
            modelPeminjaman = new Peminjaman();

            CreateCommand = new Command(async () => await CreatePeminjamanAsync());
            UpdateCommand = new Command(async () => await UpdatePeminjamanAsync());
            DeleteCommand = new Command(async () => await DeletePeminjamanAsync());
            ReadCommand = new Command(async () => await ReadPeminjamanAsync(true));
            ReadCommand.Execute(null);
        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Peminjaman> DataPeminjaman // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return dataPeminjaman;
            }

            set
            {
                SetProperty(ref dataPeminjaman, value); // property yang ada di base view models
            }
        }

        public Peminjaman ModelPeminjaman
        {
            get
            {
                return modelPeminjaman;
            }

            set
            {
                SetProperty(ref modelPeminjaman, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Peminjaman> dataPeminjaman;
        private Peminjaman modelPeminjaman;


        private async Task InitPeminjamanAsync()
        {
            var data = Peminjaman.GetAll();

            await Task.Run(() => {
                DataPeminjaman = new ObservableCollection<Peminjaman>(data);
            });
        }

        private async Task ReadPeminjamanAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitPeminjamanAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Peminjaman> ReadPeminjamanAsync(int id_peminjaman)
        {
            await Task.Delay(0);
            return DataPeminjaman.Where(model => model.id_peminjaman.Equals(id_peminjaman)).SingleOrDefault();
        }

        private async Task CreatePeminjamanAsync()
        {
            Peminjaman.Save(ModelPeminjaman);
            await ReadPeminjamanAsync(true);
        }

        private async Task UpdatePeminjamanAsync()
        {
            var data = ModelPeminjaman;
            Peminjaman.Update(data);
            await ReadPeminjamanAsync(true);
        }

        private async Task DeletePeminjamanAsync()
        {
            Peminjaman.Delete(ModelPeminjaman);
            await ReadPeminjamanAsync(true);
        }
    }
}
