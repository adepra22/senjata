﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input;
using Bci2D_App.Win10.ViewModels;
using System.Collections;
using Senjata_App.Win10;
using Senjata_App.Win10.Models;

namespace Pengembalian_App.Win10.ViewModels {
    public class PengembalianViewModel : BaseViewModel {
        koneksi koneksi;
        PengembalianDB Pengembalian;

        public PengembalianViewModel()
        {
            koneksi = koneksi.GetInstance();
            Pengembalian = new PengembalianDB(koneksi.GetConnection());

            dataPengembalian = new ObservableCollection<Pengembalian>();
            modelPengembalian = new Pengembalian();

            CreateCommand = new Command(async () => await CreatePengembalianAsync());
            UpdateCommand = new Command(async () => await UpdatePengembalianAsync());
            DeleteCommand = new Command(async () => await DeletePengembalianAsync());
            ReadCommand = new Command(async () => await ReadPengembalianAsync(true));
            ReadCommand.Execute(null);
        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Pengembalian> DataPengembalian // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return dataPengembalian;
            }

            set
            {
                SetProperty(ref dataPengembalian, value); // property yang ada di base view models
            }
        }

        public Pengembalian ModelPengembalian
        {
            get
            {
                return modelPengembalian;
            }

            set
            {
                SetProperty(ref modelPengembalian, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Pengembalian> dataPengembalian;
        private Pengembalian modelPengembalian;


        private async Task InitPengembalianAsync()
        {
            var data = Pengembalian.GetAll();

            await Task.Run(() => {
                DataPengembalian = new ObservableCollection<Pengembalian>(data);
            });
        }

        private async Task ReadPengembalianAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitPengembalianAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Pengembalian> ReadPengembalianAsync(int id_pengembalian)
        {
            await Task.Delay(0);
            return DataPengembalian.Where(model => model.id_pengembalian.Equals(id_pengembalian)).SingleOrDefault();
        }

        private async Task CreatePengembalianAsync()
        {
            Pengembalian.Save(ModelPengembalian);
            await ReadPengembalianAsync(true);
        }

        private async Task UpdatePengembalianAsync()
        {
            var data = ModelPengembalian;
            Pengembalian.Update(data);
            await ReadPengembalianAsync(true);
        }

        private async Task DeletePengembalianAsync()
        {
            Pengembalian.Delete(ModelPengembalian);
            await ReadPengembalianAsync(true);
        }
    }
}
