using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input;
using Senjata_App.Win10.Models;
using Bci2D_App.Win10.ViewModels;
using System.Collections;

namespace Senjata_App.Win10.ViewModels {
    public class SenjataViewModel : BaseViewModel {
        koneksi koneksi;
        SenjataDB Senjata;

        public SenjataViewModel()
        {
            koneksi = koneksi.GetInstance();
            Senjata = new SenjataDB(koneksi.GetConnection());

            dataSenjata = new ObservableCollection<Senjata>();
            modelSenjata = new Senjata();

            CreateCommand = new Command(async () => await CreateSenjataAsync());
            UpdateCommand = new Command(async () => await UpdateSenjataAsync());
            DeleteCommand = new Command(async () => await DeleteSenjataAsync());
            ReadCommand = new Command(async () => await ReadSenjataAsync(true));
            ReadCommand.Execute(null);
        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Senjata> DataSenjata // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return dataSenjata;
            }

            set
            {
                SetProperty(ref dataSenjata, value); // property yang ada di base view models
            }
        }

        public Senjata ModelSenjata
        {
            get
            {
                return modelSenjata;
            }

            set
            {
                SetProperty(ref modelSenjata, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Senjata> dataSenjata;
        private Senjata modelSenjata;


        private async Task InitSenjataAsync()
        {
            var data = Senjata.GetAll();

            await Task.Run(() => {
                DataSenjata = new ObservableCollection<Senjata>(data);
            });
        }

        private async Task ReadSenjataAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitSenjataAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Senjata> ReadSenjataAsync(int id_senjata)
        {
            await Task.Delay(0);
            return DataSenjata.Where(model => model.id_senjata.Equals(id_senjata)).SingleOrDefault();
        }

        private async Task CreateSenjataAsync()
        {
            Senjata.Save(ModelSenjata);
            await ReadSenjataAsync(true);
        }

        private async Task UpdateSenjataAsync()
        {
            var data = ModelSenjata;
            Senjata.Update(data);
            await ReadSenjataAsync(true);
        }

        private async Task DeleteSenjataAsync()
        {
            Senjata.Delete(ModelSenjata);
            await ReadSenjataAsync(true);
        }
    }
}
