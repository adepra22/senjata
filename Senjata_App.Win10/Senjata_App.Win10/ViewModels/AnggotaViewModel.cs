﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input;
using Senjata_App.Win10.Models;
using Bci2D_App.Win10.ViewModels;
using System.Collections;

namespace Senjata_App.Win10.ViewModels {
    public class AnggotaViewModel : BaseViewModel {
        koneksi koneksi;
        AnggotaDB Anggota;

        public AnggotaViewModel()
        {
            koneksi = koneksi.GetInstance();
            Anggota = new AnggotaDB(koneksi.GetConnection());

            dataAnggota = new ObservableCollection<Anggota>();
            modelAnggota = new Anggota();

            CreateCommand = new Command(async () => await CreateAnggotaAsync());
            UpdateCommand = new Command(async () => await UpdateAnggotaAsync());
            DeleteCommand = new Command(async () => await DeleteAnggotaAsync());
            ReadCommand = new Command(async () => await ReadAnggotaAsync(true));
            ReadCommand.Execute(null);
        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Anggota> DataAnggota // untuk menghandle collection, berapa banyak collection yang ditampilkan maka seberapa itulah observasi collection yang kamu buat?
        {
            get
            {
                return dataAnggota;
            }

            set
            {
                SetProperty(ref dataAnggota, value); // property yang ada di base view models
            }
        }

        public Anggota ModelAnggota
        {
            get
            {
                return modelAnggota;
            }

            set
            {
                SetProperty(ref modelAnggota, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Anggota> dataAnggota;
        private Anggota modelAnggota;


        private async Task InitAnggotaAsync()
        {
            var data = Anggota.GetAll();

            await Task.Run(() => {
                DataAnggota = new ObservableCollection<Anggota>(data);
            });
        }

        private async Task ReadAnggotaAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitAnggotaAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Anggota> ReadAnggotaAsync(int id_anggota)
        {
            await Task.Delay(0);
            return DataAnggota.Where(model => model.id_anggota.Equals(id_anggota)).SingleOrDefault();
        }

        private async Task CreateAnggotaAsync()
        {
            Anggota.Save(ModelAnggota);
            await ReadAnggotaAsync(true);
        }

        private async Task UpdateAnggotaAsync()
        {
            var data = ModelAnggota;
            Anggota.Update(data);
            await ReadAnggotaAsync(true);
        }

        private async Task DeleteAnggotaAsync()
        {
            Anggota.Delete(ModelAnggota);
            await ReadAnggotaAsync(true);
        }
    }
}
