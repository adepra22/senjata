using MySql.Data.MySqlClient;
using Senjata_App.Win10.Models;
using System;
using System.Collections.Generic;

namespace Senjata_App.Win10
{
    class PetugasDB
    {
        private MySqlConnection konek;
        private string strSql = string.Empty;
        public PetugasDB(MySqlConnection konek)
        {
            this.konek = konek;
        }

        private Petugas MappingRowToObject(MySqlDataReader dtr)
        {
            int id_petugas = dtr["id_petugas"] is DBNull ? 0 : Convert.ToInt32(dtr["id_petugas"].ToString());
            string nama = dtr["nama"] is DBNull ? string.Empty : dtr["nama"].ToString();
            string no_telp = dtr["no_telp"] is DBNull ? string.Empty : dtr["no_telp"].ToString();
            string username = dtr["username"] is DBNull ? string.Empty : dtr["username"].ToString();
            string password = dtr["password"] is DBNull ? string.Empty : dtr["password"].ToString();
            
            return new Petugas()
            {
                id_petugas = id_petugas,
				nama = nama,
                no_telp = no_telp,
                username = username,
                password = password
            };
        }

        public List<Petugas> GetAll() //read
        {
            List<Petugas> daftarptg = new List<Petugas>();

            strSql = "SELECT * FROM petugas";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                using (MySqlDataReader dtr = cmd.ExecuteReader())
                {
                    while (dtr.Read())
                    {
                        daftarptg.Add(MappingRowToObject(dtr));
                    }
                }
            }
            return daftarptg;
        }

        public int Save(Petugas ptg)
        {
            strSql = "INSERT INTO petugas (id_petugas, nama, no_telp, username, password) VALUES(@1, @2, @3, @4, @5)";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", ptg.id_petugas);
                cmd.Parameters.AddWithValue("@2", ptg.nama);
                cmd.Parameters.AddWithValue("@3", ptg.no_telp);
                cmd.Parameters.AddWithValue("@4", ptg.username);
                cmd.Parameters.AddWithValue("@5", ptg.password);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Delete(Petugas ptg)
        {
            strSql = "DELETE FROM petugas WHERE id_petugas = @1";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", ptg.id_petugas);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Update(Petugas ptg)
        {
            strSql = "UPDATE petugas SET id_petugas = @1, nama = @2, no_telp = @3, username = @4, password = @5 WHERE id_petugas = @8";

            using (MySqlCommand cmd = new MySqlCommand(strSql, konek))
            {
                cmd.Parameters.AddWithValue("@1", ptg.id_petugas);
                cmd.Parameters.AddWithValue("@2", ptg.nama);
                cmd.Parameters.AddWithValue("@3", ptg.no_telp);
                cmd.Parameters.AddWithValue("@4", ptg.username);
                cmd.Parameters.AddWithValue("@5", ptg.password);
				cmd.Parameters.AddWithValue("@8", ptg.id_petugas);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
